﻿public class PixelPan
{
    public static DirectionMode DIRECTION_MODE = DirectionMode.White;
    /// <summary>
    /// the bottom left pixel of the pan
    /// </summary>
    public Pixel BottomLeft;

    /// <summary>
    /// the top left pixel of the pan
    /// </summary>
    public Pixel TopLeft;

    /// <summary>
    /// the top right pixel of the pan
    /// </summary>
    public Pixel TopRight;

    /// <summary>
    /// the bottom right pixel of the pan
    /// </summary>
    public Pixel BottomRight;

    /// <summary>
    /// the direction from which the pan came from
    /// </summary>
    public Direction PanningDirection;

    /// <summary>
    /// constructor of the class
    /// </summary>
    /// <param name="bottom_left">the bottom left pixel of the pan</param>
    /// <param name="top_left">the top left pixel of the pan</param>
    /// <param name="top_right">the top right pixel of the pan</param>
    /// <param name="bottom_right">the bottom right pixel of the pan</param>
    public PixelPan (Pixel bottom_left, Pixel top_left, Pixel top_right, Pixel bottom_right)
    {
        this.BottomLeft = bottom_left;
        this.TopLeft = top_left;
        this.TopRight = top_right;
        this.BottomRight = bottom_right;
    }

    /// <summary>
    /// the combination represented in integer 0 to 15
    /// </summary>
    public int ActiveCombination
    {
        get
        {
            return BottomLeft.ActiveBit | TopLeft.ActiveBit | TopRight.ActiveBit | BottomRight.ActiveBit;
        }
    }

    /// <summary>
    /// calculate the pixels displacement
    /// </summary>
    /// <returns></returns>
    public Point2D GetCordDisplacement()
    {
        switch (PanningDirection)
        {
            case Direction.Up:
                return BottomLeft.Distance(TopLeft);
            case Direction.Right:
                return TopLeft.Distance(TopRight);
            case Direction.Left:
                return TopRight.Distance(TopLeft);
        }
        return null;
    }

    /// <summary>
    /// check the selection combination for the right direction
    /// </summary>
    /// <param name="selection">group of 4 pixels 2 by 2</param>
    /// <returns>the direction of the path</returns>
    public static Direction GetSubPathDirection(PixelPan selection)
    {

        switch (selection.ActiveCombination)
        {
            case 1:
                // turn left
                return Direction.Left;
            case 3:
                // go up
                return Direction.Up;
            case 7:
                // go right
                return Direction.Right;
            case 5:
                switch (PixelPan.DIRECTION_MODE)
                {
                    case DirectionMode.Black:
                        // group black pixels
                        return Direction.Right;
                    case DirectionMode.White:
                        // seprate black pixels
                        return Direction.Left;
                }
                return Direction.Right;
        }
        return Direction.Up;
    }

    public override string ToString()
    {
        string msg = "";
        msg += $"Bottom Left ({BottomLeft.X}, {BottomLeft.Y}) \n";
        msg += $"Top Left ({TopLeft.X}, {TopLeft.Y}) \n";
        msg += $"Top Right ({TopRight.X}, {TopRight.Y}) \n";
        msg += $"Bottom Right ({BottomRight.X}, {BottomRight.Y}) \n";
        return msg;
    }
}

public enum Direction
{
    Up,
    Right,
    Left
}

public enum DirectionMode
{
    Black,
    White
}