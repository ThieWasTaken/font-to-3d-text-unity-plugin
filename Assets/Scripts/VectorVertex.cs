﻿using UnityEngine;
public class Point2D
{
    public static Point2D One = new Point2D(1f, 1f);
    public float x;
    public float y;

    public Point2D(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public static Point2D operator +(Point2D a, Point2D b)
    {
        return new Point2D(a.x + b.x, a.y + b.y);
    }

    public static Point2D operator +(Point2D a, Pixel b)
    {
        return new Point2D(a.x + b.X, a.y + b.Y);
    }

    public static Point2D operator -(Point2D a, Point2D b)
    {
        return new Point2D(a.x - b.x, a.y - b.y);
    }

    public static Point2D operator /(Point2D a, float scaler)
    {
        return new Point2D(a.x / scaler, a.y / scaler);
    }

    public static bool operator ==(Point2D a, Point2D b)
    {
        return (a.x == b.x) && (a.y == b.y);
    }

    public static bool operator !=(Point2D a, Point2D b)
    {
        return (a.x == b.x) && (a.y == b.y);
    }

    public override string ToString()
    {
        return $"({x}, {y})";
    }

    public Vector3 ToVector3()
    {
        return new Vector3(x, y);
    }
}