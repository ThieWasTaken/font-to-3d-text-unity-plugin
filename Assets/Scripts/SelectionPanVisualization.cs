﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionPanVisualization : MonoBehaviour
{
    public Material Black;
    public Material White;
    private GameObject BottomLeft, TopLeft, TopRight, BottomRight;

    private void Start()
    {
        InitializeRenderer();
    }

    /// <summary>
    /// initializes the renderer with basic 4 unlit pixels aka quads game objects
    /// </summary>
    private void InitializeRenderer()
    {
        BottomLeft = GameObject.CreatePrimitive(PrimitiveType.Quad);
        BottomLeft.transform.position = new Vector2(-10f, 0f);
        BottomLeft.GetComponent<MeshRenderer>().material = White;

        TopLeft = GameObject.CreatePrimitive(PrimitiveType.Quad);
        TopLeft.transform.position = new Vector2(-10f, 1f);
        TopLeft.GetComponent<MeshRenderer>().material = White;

        TopRight = GameObject.CreatePrimitive(PrimitiveType.Quad);
        TopRight.transform.position = new Vector2(-9f, 1f);
        TopRight.GetComponent<MeshRenderer>().material = White;

        BottomRight = GameObject.CreatePrimitive(PrimitiveType.Quad);
        BottomRight.transform.position = new Vector2(-9f, 0f);
        BottomRight.GetComponent<MeshRenderer>().material = White;
    }

    /// <summary>
    /// renders the selection in active world scene
    /// </summary>
    /// <param name="selection">group of 4 pixels 2 by 2</param>
    public void Render(PixelPan selection)
    {
        BottomLeft.GetComponent<MeshRenderer>().material = (selection.BottomLeft.ActiveBit == 0) ? White : Black;
        TopLeft.GetComponent<MeshRenderer>().material = (selection.TopLeft.ActiveBit == 0) ? White : Black;
        TopRight.GetComponent<MeshRenderer>().material = (selection.TopRight.ActiveBit == 0) ? White : Black;
        BottomRight.GetComponent<MeshRenderer>().material = (selection.BottomRight.ActiveBit == 0) ? White : Black;
    }
}
