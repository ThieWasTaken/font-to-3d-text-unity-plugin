﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureVisualizer : MonoBehaviour
{
    /// <summary>
    /// the input texture in the editor window
    /// </summary>
    public Texture2D Texture;

    /// <summary>
    /// the alpha threshold for validating pixels
    /// </summary>
    public float AlphaThreshold;

    /// <summary>
    /// the material for active pixels
    /// </summary>
    public Material Material; 

    private void Start()
    {
        VisualizeTexture();
        VectorGraph texture_graph = GetComponent<VectorGraph>();
        texture_graph.Initialize(Texture, AlphaThreshold);
    }




    /// <summary>
    /// visualizes the input texture's pixels as quads
    /// </summary>
    void VisualizeTexture()
    {
        Color32[] pixels = Texture.GetPixels32();
        int index = 0;
        for (int y = 0; y < Texture.height; y++)
        {
            for (int x = 0; x < Texture.width; x++)
            {
                bool active_pixel = pixels[index].a > AlphaThreshold;
                if (active_pixel)
                {
                    GameObject quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
                    quad.transform.position = new Vector3(x, y, 0);
                    quad.transform.SetParent(transform);
                    quad.GetComponent<MeshRenderer>().material = Material;
                }
                index++;
            }
        }
    }
}
