﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class VectorGraph : MonoBehaviour
{
    /// <summary>
    /// the gameobject responible for rendering the selection after fixing its rotation
    /// </summary>
    public SelectionPanVisualization PanRenderer;

    /// <summary>
    /// list of all closed paths in the texture
    /// </summary>
    List<VectorPath> ClosedPaths;

    /// <summary>
    /// the active texture
    /// </summary>
    Texture2D Texture;

    /// <summary>
    /// the alpha threshold to validate active pixels
    /// </summary>
    float AlphaThreshold;

    /// <summary>
    /// a 2d array of pixels : true if active pixel
    /// </summary>
    bool[,] PixelsActiveState;

    /// <summary>
    /// the selected pixels coordination in world space
    /// </summary>
    Vector2[] SelectionCoordinations;

    bool TextureIsNotEmpty
    {
        get
        {
            foreach(bool pixel in PixelsActiveState)
            {
                if (pixel == true) return true;
            }
            return false;
        }
    }

    /// <summary>
    /// initializes the vector graph / works as a constructor
    /// </summary>
    /// <param name="texture">the texture to work on</param>
    /// <param name="alpha_threshold">the alpha threshold responsible for validating pixels activation</param>
    public void Initialize(Texture2D texture, float alpha_threshold)
    {
        this.Texture = texture;
        this.AlphaThreshold = alpha_threshold;
        this.ClosedPaths = new List<VectorPath>();
        GeneratePixels2DArray();
        StopAllCoroutines();
        StartCoroutine("GeneratePaths");
    }

    /// <summary>
    /// a coroutine to generate path over time instead of single frame
    /// </summary>
    /// <returns></returns>
    IEnumerator GeneratePaths()
    {
        PixelPan selection = GetPixelPan();
        VectorPath path = new VectorPath();
        ClosedPaths.Add(path);
        while (path.Closed == false && selection != null)
        {
            UpdateSelectionCoordinations(selection);
            PanRenderer.Render(selection);
            path.GenerateSubPath(selection);
            TranslateSelection(selection);
            yield return null;
        }
        VectorPath last_path = ClosedPaths[ClosedPaths.Count - 1];
        InvertTextureAlpha(last_path);
        if (TextureIsNotEmpty) StartCoroutine("GeneratePaths");
    }

    /// <summary>
    /// inverts the alpha of the pixels inside a path
    /// </summary>
    /// <param name="path">the path to check pixels inside</param>
    private void InvertTextureAlpha(VectorPath path)
    {
        for (int y = 0; y < Texture.height; y++)
        {
            for (int x = 0; x < Texture.width; x++)
            {
                bool pixel_is_active = PixelsActiveState[x, y];
                UpdatePixelIntersectionWithEdges(path, x, y);
            }
        }
    }

    /// <summary>
    /// invert the alpha of a single pixel if surrounded by odd edges on both side
    /// </summary>
    /// <param name="vertical_edges">a list of vertical edges</param>
    /// <param name="x">pixel x coordination</param>
    /// <param name="y">pixel y coordination</param>
    private void UpdatePixelIntersectionWithEdges(VectorPath path, int x, int y)
    {
        bool vertical_edges_on_pixel_sides_are_odds = CheckForVerticalEdges(path.verticalEdges, x, y);
        bool horizontal_edges_on_pixel_sides_are_odds = CheckForHorizontalEdges(path.HorizontalEdges, x, y);

        bool pixel_is_inside_the_path = vertical_edges_on_pixel_sides_are_odds && horizontal_edges_on_pixel_sides_are_odds;
        // if both edges are odds >> pixels is inverted
        if (pixel_is_inside_the_path)
        {
            InvertPixelActiveState(x, y);
        }
    }

    private bool CheckForVerticalEdges(Dictionary<float, List<VectorEdge>> vertical_edges, int x, int y)
    {
        bool path_doesnt_have_vertical_edges_on_pixel_position = !vertical_edges.ContainsKey(y);
        if (path_doesnt_have_vertical_edges_on_pixel_position) return false;

        List<VectorEdge> vertical_edges_at_pixel_y = vertical_edges[y];
        int right_edges = 0;
        int left_edges = 0;
        foreach (VectorEdge edge in vertical_edges_at_pixel_y)
        {
            bool edge_is_on_pixel_left_side = x > edge.start.x;
            bool edge_is_on_pixel_right_side = x < edge.start.x;
            if (edge_is_on_pixel_left_side)
            {
                left_edges++;
            }
            if (edge_is_on_pixel_right_side)
            {
                right_edges++;
            }
        }
        bool pixel_has_odd_edges_on_left_sides = (left_edges & 1) == 1;
        bool pixel_has_odd_edges_on_right_sides = (right_edges & 1) == 1;
        return pixel_has_odd_edges_on_left_sides && pixel_has_odd_edges_on_right_sides;
    }

    private bool CheckForHorizontalEdges(Dictionary<float, List<VectorEdge>> horizontal_edges, int x, int y)
    {
        bool path_doesnt_have_horizontal_edges_on_pixel_position = !horizontal_edges.ContainsKey(x);
        if (path_doesnt_have_horizontal_edges_on_pixel_position) return false;

        List<VectorEdge> vertical_edges_at_pixel_x = horizontal_edges[x];
        int lower_edges = 0;
        int upper_edges = 0;
        foreach (VectorEdge edge in vertical_edges_at_pixel_x)
        {
            bool edge_is_on_pixel_lower_side = y > edge.start.y;
            bool edge_is_on_pixel_upper_side = y < edge.start.y;
            if (edge_is_on_pixel_lower_side)
            {
                lower_edges++;
            }
            if (edge_is_on_pixel_upper_side)
            {
                upper_edges++;
            }
        }
        bool pixel_has_odd_edges_on_upper_sides = (lower_edges & 1) == 1;
        bool pixel_has_odd_edges_on_lower_sides = (upper_edges & 1) == 1;
        return pixel_has_odd_edges_on_upper_sides && pixel_has_odd_edges_on_lower_sides;
    }

    /// <summary>
    /// inverts the alpha of a single pixel
    /// </summary>
    /// <param name="x">column of the pixel in the texture</param>
    /// <param name="y">row of the pixel in the texture</param>
    private void InvertPixelActiveState(int x, int y)
    {
        bool pixel_state = PixelsActiveState[x, y];
        PixelsActiveState[x, y] = !pixel_state;
    }


    /// <summary>
    /// updates the array holding the selected pixels coordinations in real world space
    /// </summary>
    /// <param name="selection">group of 4 pixels 2 by 2</param>
    void UpdateSelectionCoordinations(PixelPan selection)
    {
        SelectionCoordinations = new Vector2[4];
        SelectionCoordinations[0] = new Vector2(selection.BottomLeft.X, selection.BottomLeft.Y);
        SelectionCoordinations[1] = new Vector2(selection.TopLeft.X, selection.TopLeft.Y);
        SelectionCoordinations[2] = new Vector2(selection.TopRight.X, selection.TopRight.Y);
        SelectionCoordinations[3] = new Vector2(selection.BottomRight.X, selection.BottomRight.Y);
    }

    private void OnDrawGizmosSelected()
    {
        if (SelectionCoordinations == null || ClosedPaths == null) return;
        for(int i=0; i<SelectionCoordinations.Length; i++)
        {
            Gizmos.color = Color.white;
            if(i < SelectionCoordinations.Length - 1)
            {
                Gizmos.DrawLine(SelectionCoordinations[i], SelectionCoordinations[i + 1]);
            }
            else
            {
                Gizmos.DrawLine(SelectionCoordinations[i], SelectionCoordinations[0]);
            }
            
        }
        Gizmos.color = Color.red;
        Gizmos.DrawCube(SelectionCoordinations[3], Vector3.one * 0.5f); //Bottom Left
        Gizmos.DrawCube(SelectionCoordinations[0], Vector3.one * 0.5f); //Bottom Left
        Gizmos.DrawCube(SelectionCoordinations[1], Vector3.one * 0.5f); //Bottom Left

        for(int path_index = 0; path_index < ClosedPaths.Count; path_index++)
        {
            for (int vertex_index = 0; vertex_index < ClosedPaths[path_index].vertices.Count; vertex_index++)
            {
                Gizmos.color = Color.red;
                if (vertex_index < ClosedPaths[path_index].vertices.Count - 1)
                {
                    Gizmos.DrawLine(ClosedPaths[path_index].vertices[vertex_index].ToVector3(), ClosedPaths[path_index].vertices[vertex_index + 1].ToVector3());
                }
                else
                {
                    Gizmos.DrawLine(ClosedPaths[path_index].vertices[vertex_index].ToVector3(), ClosedPaths[path_index].vertices[0].ToVector3());
                }
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(ClosedPaths[path_index].vertices[vertex_index].ToVector3(), 0.25f);
            }
        }
    }

    /// <summary>
    /// translate a selection based on the direction saved in the selection
    /// </summary>
    /// <param name="selection">group of 4 pixels 2 by 2</param>
    public void TranslateSelection(PixelPan selection)
    {
        Point2D displacement = selection.GetCordDisplacement();
        int x_displacement = (int) displacement.x;
        int y_displacement = (int) displacement.y;

        Pixel translated_bottom_left = TranslatePixel(selection.BottomLeft, x_displacement, y_displacement);
        Pixel translated_top_left = TranslatePixel(selection.TopLeft, x_displacement, y_displacement);
        Pixel translated_top_right = TranslatePixel(selection.TopRight, x_displacement, y_displacement);
        Pixel translated_bottom_right = TranslatePixel(selection.BottomRight, x_displacement, y_displacement);

        PixelPan new_selection = new PixelPan(translated_bottom_left, translated_top_left, translated_top_right, translated_bottom_right);

        switch (selection.PanningDirection)
        {
            case Direction.Left:
                RotateSelectionBasedOnLeftDirection(selection, new_selection);
                break;
            case Direction.Up:
                RotateSelectionBasedOnUpDirection(selection, new_selection);
                break;
            case Direction.Right:
                RotateSelectionBasedOnRightDirection(selection, new_selection);
                break;
        }
        UpdateNewSelectionActiveState(selection);
    }

    /// <summary>
    /// fetch the selected pixels active state.
    /// </summary>
    /// <param name="selection">group of 4 pixels 2 by 2</param>
    private void UpdateNewSelectionActiveState(PixelPan selection)
    {
        selection.BottomLeft.ActiveBit = PixelsActiveState[selection.BottomLeft.X, selection.BottomLeft.Y] ? 1 : 0;
        selection.TopLeft.ActiveBit = PixelsActiveState[selection.TopLeft.X, selection.TopLeft.Y] ? 2 : 0;
        selection.TopRight.ActiveBit = PixelsActiveState[selection.TopRight.X, selection.TopRight.Y] ? 4 : 0;
        selection.BottomRight.ActiveBit = PixelsActiveState[selection.BottomRight.X, selection.BottomRight.Y] ? 8 : 0;
    }

    /// <summary>
    /// move selection left by 1 pixel
    /// </summary>
    /// <param name="old_selection">the old pixels</param>
    /// <param name="new_selection">the new pixels</param>
    private void RotateSelectionBasedOnLeftDirection(PixelPan old_selection, PixelPan new_selection)
    {
        old_selection.BottomLeft = new_selection.BottomRight;
        old_selection.TopLeft = new_selection.BottomLeft;
        old_selection.TopRight = new_selection.TopLeft;
        old_selection.BottomRight = new_selection.TopRight;
    }

    /// <summary>
    /// move selection up by 1 pixel
    /// </summary>
    /// <param name="old_selection">the old pixels</param>
    /// <param name="new_selection">the new pixels</param>
    private void RotateSelectionBasedOnUpDirection(PixelPan old_selection, PixelPan new_selection)
    {
        old_selection.BottomLeft = new_selection.BottomLeft;
        old_selection.TopLeft = new_selection.TopLeft;
        old_selection.TopRight = new_selection.TopRight;
        old_selection.BottomRight = new_selection.BottomRight;
    }

    /// <summary>
    /// move selection right by 1 pixel
    /// </summary>
    /// <param name="old_selection">the old pixels</param>
    /// <param name="new_selection">the new pixels</param>
    private void RotateSelectionBasedOnRightDirection(PixelPan old_selection, PixelPan new_selection)
    {
        old_selection.BottomLeft = new_selection.TopLeft;
        old_selection.TopLeft = new_selection.TopRight;
        old_selection.TopRight = new_selection.BottomRight;
        old_selection.BottomRight = new_selection.BottomLeft;
    }

    /// <summary>
    /// translate a pixel by an amount in x and y cords of the texture
    /// </summary>
    /// <param name="pixel">the pixel to be translated</param>
    /// <param name="x_displacement">the translation amount in the x axis</param>
    /// <param name="y_displacement">the translation amount in the y axis</param>
    /// <returns>the new pixel</returns>
    Pixel TranslatePixel(Pixel pixel, int x_displacement, int y_displacement)
    {
        int new_x = pixel.X + x_displacement;
        int new_y = pixel.Y + y_displacement;
        return new Pixel(new_x, new_y, 0);
    }

    /// <summary>
    /// find the first black pixel on the texture and select it along with its adjacent pixels
    /// </summary>
    /// <returns>PixelPan of 4 pixels selected</returns>
    PixelPan GetPixelPan()
    {
        for (int y = 0; y < Texture.height; y++)
        {
            for (int x = 0; x < Texture.width; x++)
            {
                if (PixelsActiveState[x, y])
                {
                    Pixel bottom_left = new Pixel(x, y, PixelsActiveState[x, y] ? 1 : 0);
                    Pixel top_left = new Pixel(x, y - 1, PixelsActiveState[x, y - 1] ? 2 : 0);
                    Pixel top_right = new Pixel(x - 1, y - 1, PixelsActiveState[x-1, y-1] ? 4 : 0);
                    Pixel bottom_right = new Pixel(x - 1, y, PixelsActiveState[x - 1, y] ? 8 : 0);
                    return new PixelPan(bottom_left, top_left, top_right, bottom_right);
                }
            }
        }
        return null;
    }

    /// <summary>
    /// generates the 2d array of occupance state for the pixel: true if pixel is active
    /// </summary>
    void GeneratePixels2DArray()
    {
        Color32[] colors = Texture.GetPixels32();
        int index = 0;
        PixelsActiveState = new bool[Texture.width, Texture.height];
        for (int y = 0; y < Texture.height; y++)
        {
            for (int x = 0; x < Texture.width; x++)
            {
                PixelsActiveState[x, y] = colors[index].a >= AlphaThreshold;
                index++;
            }
        }
    }
}
