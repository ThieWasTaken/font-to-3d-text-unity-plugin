﻿public class VectorEdge
{
    public Point2D start;
    public Point2D tip;

    public VectorEdge(Point2D start, Point2D tip)
    {
        this.start = start;
        this.tip = tip;
    }

    public static bool operator ==(VectorEdge a1, VectorEdge a2)
    {
        if (a1.start == a2.start && a1.tip == a2.tip) return true;
        if (a1.start == a2.tip && a1.tip == a2.start) return true;
        return false;
    }

    public static bool operator !=(VectorEdge a1, VectorEdge a2)
    {
        if (a1.start == a2.start && a1.tip == a2.tip) return false;
        if (a1.start == a2.tip && a1.tip == a2.start) return false;
        return true;
    }

    public override bool Equals(object obj)
    {
        return (VectorEdge) obj == this;
    }
}