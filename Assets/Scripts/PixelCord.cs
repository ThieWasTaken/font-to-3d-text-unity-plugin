﻿public class Pixel
{
    public int X, Y;
    public int ActiveBit;
    public Pixel (int x, int y, int active_state)
    {
        this.X = x;
        this.Y = y;
        this.ActiveBit = active_state;
    }

    public void Update(int x, int y, int active_state)
    {
        this.X = x;
        this.Y = y;
        this.ActiveBit = active_state;
    }

    public Point2D Distance(Pixel target)
    {
        float x = (float) target.X - (float) X;
        float y = (float) target.Y - (float) Y;
        return new Point2D(x, y);
    }
}

