﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// path of points
/// </summary>
public class VectorPath
{
    /// <summary>
    /// points of the path
    /// </summary>
    public List<Point2D> vertices;

    /// <summary>
    /// all vertical sub-paths of the path with their middle points (y) as the key
    /// </summary>
    public Dictionary<float, List<VectorEdge>> verticalEdges;

    /// <summary>
    /// all horizontal sub-paths of the path with their middle points (x) as the key
    /// </summary>
    public Dictionary<float, List<VectorEdge>> HorizontalEdges;

    /// <summary>
    /// the class contructor
    /// </summary>
    public VectorPath()
    {
        this.vertices = new List<Point2D>();
        this.verticalEdges = new Dictionary<float, List<VectorEdge>>();
        this.HorizontalEdges = new Dictionary<float, List<VectorEdge>>();
    }

    /// <summary>
    /// extract the sub-path from the selection based on the selection active pixels
    /// </summary>
    /// <param name="selection">group of 4 pixels 2 by 2</param>
    public void GenerateSubPath(PixelPan selection)
    {
        Direction direction = PixelPan.GetSubPathDirection(selection);
        Point2D sub_path_start = CalculateSubPathStartPoint(selection);
        Point2D sub_path_end = CalculateSubPathEndPoint(direction, sub_path_start, selection);
        RegisterSubPath(sub_path_start, sub_path_end);
        RegisterVerticalEdge(sub_path_start, sub_path_end);
        RegisterHorizontalEdge(sub_path_start, sub_path_end);
        selection.PanningDirection = direction;
    }

    /// <summary>
    /// register the sub-path vertices into the vertices
    /// </summary>
    /// <param name="sub_path_start">the starting point of the sub-path</param>
    /// <param name="sub_path_end">the ending point of the sub-path</param>
    private void RegisterSubPath(Point2D sub_path_start, Point2D sub_path_end)
    {
        if (vertices.Count == 0)
        {
            vertices.Add(sub_path_start);
        }
        vertices.Add(sub_path_end);
    }

    /// <summary>
    /// register the sup-path as a vertical edge if it's a vertical sub-path
    /// </summary>
    /// <param name="start">the starting point of the sub-path</param>
    /// <param name="end">the end point of the sub-path</param>
    private void RegisterVerticalEdge(Point2D start, Point2D end)
    {
        bool is_vertical_edge = start.x == end.x;
        if (is_vertical_edge)
        {
            float sup_path_mid_point = (end.y - start.y) / 2f + start.y;
            VectorEdge vertical_edge = new VectorEdge(start, end);
            if (verticalEdges.ContainsKey(sup_path_mid_point))
            {
                if (verticalEdges[sup_path_mid_point].Contains(vertical_edge))
                {
                    return;
                }
            }
            else
            {
                verticalEdges[sup_path_mid_point] = new List<VectorEdge>();
            }
            verticalEdges[sup_path_mid_point].Add(vertical_edge);
        }
    }

    /// <summary>
    /// register the sup-path as a horizontal edge if it's a horizontal sub-path
    /// </summary>
    /// <param name="start">the starting point of the sub-path</param>
    /// <param name="end">the end point of the sub-path</param>
    private void RegisterHorizontalEdge(Point2D start, Point2D end)
    {
        bool is_horizontal_edge = start.y == end.y;
        if (is_horizontal_edge)
        {
            float sup_path_mid_point = (end.x - start.x) / 2f + start.x;
            VectorEdge horizontal_edge = new VectorEdge(start, end);
            if (HorizontalEdges.ContainsKey(sup_path_mid_point))
            {
                if (HorizontalEdges[sup_path_mid_point].Contains(horizontal_edge))
                {
                    return;
                }
            }
            else
            {
                HorizontalEdges[sup_path_mid_point] = new List<VectorEdge>();
            }
            HorizontalEdges[sup_path_mid_point].Add(horizontal_edge);
        }
    }

    /// <summary>
    /// calculate the sub-path ending point
    /// </summary>
    /// <param name="direction">the direction of the sub-path</param>
    /// <param name="start">the starting point of the path</param>
    /// <param name="selection">group of 4 pixels 2 by 2</param>
    /// <returns>the end point of the sub-path</returns>
    private Point2D CalculateSubPathEndPoint(Direction direction, Point2D start, PixelPan selection)
    {
        Point2D end = null;
        switch (direction)
        {
            case Direction.Up:
                end = start + selection.BottomLeft.Distance(selection.TopLeft);
                break;
            case Direction.Right:
                end = start + selection.BottomLeft.Distance(selection.BottomRight);
                break;
            case Direction.Left:
                end = start - selection.BottomLeft.Distance(selection.BottomRight);
                break;
        }
        return end;
    }

    /// <summary>
    /// calculates the sub-path starting point
    /// </summary>
    /// <param name="selection">group of 4 pixels 2 by 2</param>
    /// <returns>the start point of the sub-path</returns>
    private Point2D CalculateSubPathStartPoint(PixelPan selection)
    {
        return (selection.BottomLeft.Distance(selection.TopRight)) / 2f + selection.BottomLeft;
    }

    public bool Closed
    {
        get
        {
            if (vertices.Count == 0) return false;
            return Start == End;
        }
    }
    public Point2D Start
    {
        get
        {
            return vertices[0];
        }
    }
    public Point2D End
    {
        get
        {
            return vertices[vertices.Count - 1];
        }
    }
    public int Length
    {
        get
        {
            return vertices.Count - 1;
        }
    }
}